var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

var path = require('path');

//Tomar librerias desde la raiz de la aplicación
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log("Ejecutando polymerdesdenode en puerto: "+port);

app.get('/', function(req,res){
  res.sendFile('index.html', {root: '.'});
})
